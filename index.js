
const FIRST_NAME = "Andrei";
const LAST_NAME = "Neacsiu";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
  var emptyObject={};
  var cache={};
  cache.pageAccessCounter=function(websiteName='home')
  {
    websiteName=websiteName.toLowerCase();
    if(emptyObject[websiteName]===undefined)
        emptyObject[websiteName]=1;
    else
        emptyObject[websiteName]++;
  }
  cache.getCache=function()
  {
    return emptyObject;
  }
  return cache;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

